function toggleMenu() {
    $(".tggl-menu").click(function() {
        $("body").toggleClass("hide-menu");
        $("body").toggleClass("show-menu");
        if ($("body").hasClass("show-menu")) {
            $(".tggl-menu").attr("aria-expanded", "true");
			$(".tggl-menu.visually-hidden-focusable").attr("tabindex", "0");
        } else {
            $(".tggl-menu").attr("aria-expanded", "false");
			$(".tggl-menu.visually-hidden-focusable").attr("tabindex", "-1");
        }
    });
}

// Hide header on scroll down, show header on scroll up
function headerScroll() {

	var didScroll;
	var lastScrollTop = 0;
	var delta = 1;
	var navbarHeight = $('#header').outerHeight() / 3;

	$(window).scroll(function(event){
		didScroll = true;
	});

	setInterval(function() {
		if (didScroll) {
			hasScrolled();
			didScroll = false;
		}
	}, 50);

	function hasScrolled() {
		var st = $(this).scrollTop();

		if(Math.abs(lastScrollTop - st) <= delta)
			return;

		if (st > lastScrollTop && st > navbarHeight){
			// Scroll Down
			$('#header').removeClass('scroll-up').removeClass('scroll-top').addClass('scroll-down');
		} else {
			// Scroll Up
			if(st + $(window).height() < $(document).height()) {
				$('#header').removeClass('scroll-down').addClass('scroll-up');
			}
		}

		if(st < navbarHeight){
			$('#header').removeClass('scroll-up').addClass('scroll-top');
		}

		lastScrollTop = st;
	}
}
