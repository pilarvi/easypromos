class MembersMovement {
    MAX_VERTEX = 8;
    ANGLE = 360 / this.MAX_VERTEX;
    REFERENCE_VERTEX = (this.ANGLE / 2) * (1 - this.MAX_VERTEX);
    CSS_CLASSES = ['up-left', 'up', 'up-right', 'right', 'down-right', 'down', 'down-left', 'left'];
    sectors = [];
    members = [];

    constructor() {
        this.generateSectors();
        this.getMembers();
        window.addEventListener('mousemove', event => this.getMousePosition(event));
    }

    generateSectors() {
        for(let i = 0; i < this.MAX_VERTEX; i++) {
            const SHIFTED_ANGLE = i * this.ANGLE;
            this.sectors.push({ 
                className: this.CSS_CLASSES[i], 
                func: (value) => this.isBetweenVertexs(value, SHIFTED_ANGLE + this.REFERENCE_VERTEX, SHIFTED_ANGLE + this.REFERENCE_VERTEX + this.ANGLE)
            });
        }
    }

    isBetweenVertexs(value, min, max) {
        if(max > 180) {
            return value >= min && value <= max || -value >= min && -value <= max;
        }
        return value >= min && value <= max;
    }

    getMembers() {
        document.querySelectorAll('.member-img').forEach(member => {
            this.members.push(member.id);
        });
    }

    getMousePosition(event) {
        this.members.forEach((member) => {
            const imgCenteredCoordination = this.getCenterImage(member);
            const angle = Math.atan2(event.clientY - imgCenteredCoordination.y, event.clientX - imgCenteredCoordination.x) * (180 / Math.PI);

            this.sectors.forEach(sector => {
                if(sector.func(angle)) {
                    document.querySelector(`#${member}>img`).className = sector.className;
                }
            });
        });
    }

    getCenterImage(member) {
        const imgSizePosition = document.querySelector(`#${member}`).getBoundingClientRect();
        return {
            x: imgSizePosition.left + imgSizePosition.width * 0.5, 
            y: imgSizePosition.top + imgSizePosition.height * 0.5
        }
    }
}

$(window).load(() => new MembersMovement);