$(document).ready( function() {
	
	toggleMenu();
	
	if ($(window).width() >= 1280) {
		headerScroll();
	}
	
	if ($('select').length) {
		$('select').selectric();
	}
	
	var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
	var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
		return new bootstrap.Tooltip(tooltipTriggerEl)
	});
	
});